import 'package:flutter/material.dart';
import 'package:country_state_city_picker/country_state_city_picker.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Input me",
      home: Scaffold(
        appBar: AppBar(title: Text('input form v.0.0.1')),
        body: InputScreen(),
      ),
    );
  }

}

class InputScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputState();
  }

}

class InputState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String surname;
  late String countryValue;
  late String stateValue;
  late String cityValue;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldSurname(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            SelectState(
              onCountryChanged: (value) {
                setState(() {
                  countryValue = value;
                });
              },

              onStateChanged:(value) {
                setState(() {
                  stateValue = value;
                });
              },
              onCityChanged:(value) {
                setState(() {
                  cityValue = value;
                });
              },

            ),
            inputButton(),
          ],
        ),
      )

    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return 'Pls input valid email.';
        }
        if (value!.length < 1) {
          return 'Pls input valid email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldSurname() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Surname and middle name'
      ),
      validator: (value) {
        if (value!.length < 2) {
          return 'Password must be at least 2 characters.';
        }
        if (!value.contains(RegExp(r"[A-Z]"))) {
          return 'Password must be at least 1 upper-case letter characters.';
        }
        if (!value.contains(RegExp(r"[a-z]"))) {
          return 'Password must be at least 1 normal characters.';
        }
        return null;
      },
      onSaved: (value) {
        surname = value as String;
      },
    );
  }



  Widget inputButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, Demo only: $surname');
          }
        },
        child: Text('Input')
    );
  }
}